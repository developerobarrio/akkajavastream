package es.nasa.akka.streams.primenumbers;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.event.Logging;
import akka.stream.Attributes;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletionStage;

public class BigPrimes {
    public static void main(String[] args) {
        ActorSystem actorSystem = ActorSystem.create(Behaviors.empty(),"actorSystem");
                   // actorSystem.logConfiguration();
        Source<Integer, NotUsed>  source = Source.range(1,10);
        Flow<Integer, BigInteger,NotUsed> flow  = Flow.of(Integer.class).map(in->new BigInteger(8,new Random()));
        Flow<BigInteger, BigInteger,NotUsed> flowPrime  = Flow.of(BigInteger.class)
                                                              .map(in-> in.nextProbablePrime())
                                                              .log("Numero primo")
                                                              .withAttributes(
                                                                Attributes.createLogLevels(
                                                                        Logging.InfoLevel(), // onElement
                                                                        Logging.InfoLevel(), // onFinish
                                                                        Logging.DebugLevel() // onFailure
                                                                )) ;
        Flow<BigInteger, List<BigInteger>,NotUsed> flowGroup  = Flow.of(BigInteger.class)
                                                               .grouped(10)
                                                               .map(list->{
                                                                        List<BigInteger> result = new ArrayList<>(list);
                                                                                    Collections.sort(result);
                                                                                    return result;
                                                               });
        Sink<List<BigInteger>, CompletionStage<Done>> sink = Sink.foreach(System.out::println);

        source.via(flow).via(flowPrime).via(flowGroup).to(sink).run(actorSystem);

    }
}
