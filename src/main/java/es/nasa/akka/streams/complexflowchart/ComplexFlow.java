package es.nasa.akka.streams.complexflowchart;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.scaladsl.Behaviors;
import akka.japi.function.Function2;
import akka.stream.*;
import akka.stream.javadsl.*;

import java.util.concurrent.CompletionStage;


public class ComplexFlow {

    public static void main(String[] args) {
        ActorSystem actorSystem = ActorSystem.create(Behaviors.empty(),"actorSystem");

        Source<Integer, NotUsed> source   = Source.range(1,10);

        Flow<Integer,Integer,NotUsed> flow1 = Flow.of(Integer.class).map(x->x*2);
        Flow<Integer,Integer,NotUsed> flow2 = Flow.of(Integer.class).map(x->x+5);

        Sink<Integer, CompletionStage<Done>> sink = Sink.foreach(System.out::println);

        Function2< GraphDSL.Builder<CompletionStage<Done>>,
                   SinkShape<Integer>,
                   ClosedShape > shapeFunction = (GraphDSL.Builder<CompletionStage<Done>> builder,  SinkShape<Integer> out) -> {
                                                            SourceShape<Integer> sourceShape = builder.add(source);
                                                            FlowShape<Integer,Integer> flow1Shape = builder.add(flow1);
                                                            FlowShape<Integer,Integer> flow2Shape = builder.add(flow2);
                                                            UniformFanOutShape<Integer,Integer> broadcast = builder.add(Broadcast.create(2));
                                                            UniformFanInShape<Integer,Integer> merge = builder.add(Merge.create(2));

                                                            builder.from(sourceShape)
                                                                    .viaFanOut(broadcast);

                                                            builder.from(broadcast.out(0))
                                                                    .via(flow1Shape);

                                                            builder.from(broadcast.out(1))
                                                                    .via(flow2Shape);

                                                            builder.from(flow1Shape)
                                                                    .toInlet(merge.in(0));

                                                            builder.from(flow2Shape)
                                                                    .toInlet(merge.in(1));

                                                            builder.from(merge).to(out);

                                                            return ClosedShape.getInstance();
                                                         };

        Graph<ClosedShape,CompletionStage<Done>>    graph =   GraphDSL.create(sink, shapeFunction);
        RunnableGraph<CompletionStage<Done>>  rgraph = RunnableGraph.fromGraph(graph);
                                              rgraph.run(actorSystem);
    }
}
