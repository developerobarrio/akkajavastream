package es.nasa.akka.streams.graphdsl;
record  VehicleSpeed(int vehicleId, double speed) {}
