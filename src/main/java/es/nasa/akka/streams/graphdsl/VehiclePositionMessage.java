package es.nasa.akka.streams.graphdsl;

import java.util.Date;

record VehiclePositionMessage(int vehicleId, Date currentDateTime, int longPosition, int latPosition) {}